var express = require('express'),
	bodyParser = require('body-parser'),
	path = require('path'),
	app = express();

var isProduction = process.env.NODE_ENV === 'production';
var port = isProduction ? process.env.PORT : 3001;
var publicPath = path.join(__dirname, 'client');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(publicPath));

app.get('*', function(req, res) {
	var homePath = path.join(publicPath, 'index.html');
	res.sendFile(homePath);
})

app.listen(port, function() {
    console.log('AnniesBooks server listening on port ' + port);
});
