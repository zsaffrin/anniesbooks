var Webpack = require('webpack'),
    path = require('path');

var sourcePath = path.join(__dirname, 'src', 'index.js'),
    clientPath = path.join(__dirname, 'client');


var config = {
    devtool: 'eval',
    entry: { 
        app: [sourcePath] 
    },
    output: {
        path: clientPath,
        filename: 'app-bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: ['babel-loader'],
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            }
        ]
    },
    plugins: [
        new Webpack.optimize.OccurrenceOrderPlugin()
    ]
};

module.exports = config;
