import React from 'react'
import GoogleMap from './GoogleMap'

class LocationPage extends React.Component {
	constructor(props) {
		super(props)
		this.state = { 
			location: {} 
		}

		this.fetchLocationData = this.fetchLocationData.bind(this)
	}

	componentDidMount() {
		let data = this.fetchLocationData()
		data.map(location => {
			if (location.key == this.props.match.params.key) {
				this.setState({ location: location })
			}
		})
	}

	fetchLocationData() {
		return require('../data/locations.json')
	}

	render() {
		let location = this.state.location

		let address2 = null
		if (location.address2) {
			address2 = <div>{location.address2}</div>
		}

		let map = null
		if (location.map && location.map.lat && location.map.long) {
			let center = {
				lat: Number(location.map.lat),
				lng: Number(location.map.long)
			}
			let zoom = 15
			let locationName = 'Annies Books ' + location.name
			map = <div className="mb2 relative" >
				<GoogleMap google={window.google} center={center} zoom={zoom} style={{ height: '300px' }} />
			</div>
		}

		let extMapLink = null
		if (location.map && location.map.ext_url) {
			extMapLink = <div className="mb1">
				<a href={location.map.ext_url} target="_blank" className="red hover-red-light">
					<i className="fa fa-fw fa-map-marker fa-lg mr1" />
					Full Map &amp; Directions
				</a>
			</div>
		}

		let email = null
		if (location.address2) {
			email = <div className="mb1">
				<a href={'mailto:' + location.email} className="red hover-red-light">
					<i className="fa fa-fw fa-envelope-o fa-lg mr1" />
					{location.email}
				</a>
			</div>
		}

		let phone = null
		if (location.phone) {
			phone = <div className="mb1">
				<i className="fa fa-fw fa-phone fa-lg mr1" />
				{location.phone}
			</div>
		}

		return (
			<div className="pb4">
				<div className="container p2">
					
					<div className="md-col md-col-6 p1">
						<div className="mb2">
							<h2 className="caps">{location.city}, {location.state}</h2>
							<div>
								<div>{location.address1}</div>
								{address2}
								{location.city}, {location.state} {location.zip}
							</div>
						</div>

						{map}

						<div className="mb2 bold caps size-sm">
							{extMapLink}
							{email}
							{phone}
						</div>
					</div>

					<div className="md-col md-col-6 p1">
						<div className="border rounded border-red m2 p2 center">
							Please call for current days and times of operation
						</div>
					</div>

					


				</div>
			</div>
		)
	}
}

export default LocationPage
