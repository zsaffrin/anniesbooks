import React from 'react'
import ReactDOM from 'react-dom'

class GoogleMap extends React.Component {
	constructor(props) {
		super(props)

		this.loadMap = this.loadMap.bind(this)
	}
	componentDidMount() {
		this.loadMap()
	}

	loadMap() {
		let { google, center, zoom } = this.props
		let maps = google.maps
		let { lat, lng } = center

		let mapCenter = new maps.LatLng(lat, lng)
		let mapConfig = Object.assign({}, {
			center: mapCenter,
			zoom: zoom
		})

		let mapRef = this.refs.map
		let node = ReactDOM.findDOMNode(mapRef)

		this.map = new maps.Map(node, mapConfig)
		this.marker = new maps.Marker({
			position: mapCenter,
			map: this.map
		})
	}

	render() {
		return (
			<div ref='map' style={this.props.style}>
				Loading map...
			</div>
		)
	}
}

export default GoogleMap
