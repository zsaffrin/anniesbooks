import React from 'react'

class About extends React.Component {
	render() {
		return (
			<div id="home">
				<div className="container p2">
					
					<h2 className="caps">About Us</h2>
					
			        <p>In the 1970s, entrepreneur Anne Tryon Adams opened her first of three used 
			            bookstores in Westborough, MA called &quot;ANNIE’S BOOK SWAP&quot;.  By 1980, requests 
			            were coming from customers who wanted their own bookstore in their area. </p>

			        <p>Thus, the franchise corporation was formed and by the end of 1981, Ms. Adams 
			            had sold twelve franchises throughout New England.  Around 1984, the name was 
			            altered to &quot;ANNIE’S BOOK STOP&quot; and stores were selling new books and 
			            merchandise as well as the proverbial used books. Anne&apos;s motto was: &quot;I gave a 
			            Saks Fifth Avenue look to a bargain basement business.&quot;</p>

			        <p>By the end of the 1980s, franchise stores had grown to seventy strong, as far 
			            south as Florida and west to California. However, in 1990, the franchise 
			            corporation suffered financial problems and landed in the bankruptcy court.  But 
			            a strong Owners Association bought the trade name and trademarks and 
			            continued with business as usual.</p>

			        <p>Today, due to widespread difficulties in the book industry, the Annie&apos;s Book Stop 
			            numbers have shrunk, but those stores still in business continue to carry on the 
			            tradition of providing outstanding customer service.  Please click on &quot;LOCATIONS&quot; 
			            for the store nearest you.</p>

			        <p>Anyone interested in owning an Annie&apos;s Book Stop should contact individual 
			            stores for possible locations for sale.</p>

				</div>
			</div>
		)
	}
}

export default About
