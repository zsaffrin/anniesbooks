import React from 'react'
import { Link } from 'react-router-dom'

class Locations extends React.Component {
	constructor(props) {
		super(props)
		this.state = { locations: [] }

		this.fetchLocationData = this.fetchLocationData.bind(this)
	}

	componentDidMount() {
		let data = this.fetchLocationData()
		this.setState({
			locations: data
		})
	}

	fetchLocationData() {
		return require('../data/locations.json')
	}

	render() {
		return (
			<div id="home">
				<div className="container p2">
					
					<h2 className="caps mb2">Locations</h2>

					{this.state.locations.map(location => 
						<Link to={{
								pathname: '/location/' + location.key,
								data: location
							}} 
							className="block bold border rounded border-gray-lighter hover-border-red-darker red hover-red-light m1 p1" 
							key={location.id}>
							{location.city}, {location.state}
						</Link>
					)}

				</div>
			</div>
		)
	}
}

export default Locations
