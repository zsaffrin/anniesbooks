import React from 'react'

class Home extends React.Component {
	render() {
		return (
			<div id="home">
				<div className="container center p2">
					
					<h2 className="caps">Welcome to Annie&apos;s Book Stop Online</h2>
					
					<hr width="50%" className="my3" />
					
					<div className="md-col md-col-4 p2">
						<h3>BUY</h3>
						<p>New and used books at great prices! Discounts on new releases! 50% off on pre-read paperbacks!</p>
					</div>
					<div className="md-col md-col-4 p2">
						<h3>FIND</h3>
						<p>Old favorites in and out of print! Sci-fi, mystery, romance, suspense, westerns, and the ever-popular other! Let our book-loving staff help you get all the backlist of your must read authors!</p>
					</div>
					<div className="md-col md-col-4 p2">
						<h3>TRADE</h3>
						<p>Trade in your paperbacks for store credit to make your new selections even cheaper!</p>
					</div>

				</div>
			</div>
		)
	}
}

export default Home
