import React from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

import Home from './Home'
import Locations from './Locations'
import LocationPage from './LocationPage'
import About from './About'

require('../style/index.scss')

class App extends React.Component {
	render() {
		return (
			<Router>
				<div id="app" className="pb4">

					<div id="banner" className="py2 white bg-red overflow-hidden">
						<div className="container center p2">
							<div>
								<Link to="/">
									<img src={'/img/anniesbookstop-logo.png'} />
								</Link>
								<h1 className="caps m0">Annies Book Stop</h1>
							</div>
							
							<div className="caps h3 mb2" style={{ fontWeight: 400 }}>Today&apos;s Books at Yesterday&apos;s Prices</div>
							<div id="nav">
								<ul className="no-style">
									<li className="inline-block">
										<Link to="/" className="button inverted m1">Home</Link>
									</li>
									<li className="inline-block">
										<Link to="/locations" className="button inverted m1">Locations</Link>
									</li>
									<li className="inline-block">
										<Link to="/about" className="button inverted m1">About Us</Link>
									</li>
								</ul>
							</div>
						</div>
					</div>
					
					<div id="content" className="py2 red-dark overflow-hidden">
						
						<Route exact path="/" component={Home} />
						<Route exact path="/locations" component={Locations} />
						<Route path="/location/:key" component={LocationPage} />
						<Route path="/about" component={About} />

					</div>

				</div>
			</Router>
		)
	}
}

export default App
